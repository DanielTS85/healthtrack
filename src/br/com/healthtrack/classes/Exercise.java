package br.com.healthtrack.classes;

public class Exercise {
	
	private String name;
	private int repetition;
	private int moviment;
	private String type;
	
	
	public Exercise(String aName, int aRepetition, int aMoviment, String aType) {
		this.name = aName;
		this.repetition = aRepetition;
		this.moviment = aMoviment;
		this.type = aType;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getRepetition() {
		return this.repetition;
	}
	
	public int getMoviment() {
		return this.moviment;
	}
	
	public String getType() {
		return this.type;
	}
	
	public void show() {
		System.out.println(String.format("Tipo: %s", getType()));
		System.out.println(String.format("Nome do exerc�cio: %s", getName()));
		System.out.println(String.format("Nome do repeti��es: %s", getRepetition()));
		System.out.println(String.format("Nome do Movimentos: %s", getMoviment()));
	}
	
	
}
