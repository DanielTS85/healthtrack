package br.com.healthtrack.classes;

public class ExerciseEquipment extends Exercise {

	private double load;
	
	public ExerciseEquipment(String aName, int aRepetition, int aMoviment, double aLoad) {
		super(aName, aRepetition, aMoviment, "Equipamento");
		this.load = aLoad;	
	}
	
	public double getLoad() {
		return this.load;
	}
	
	@Override
	public void show() {
		super.show();
		System.out.println(String.format("Carga: %s", getLoad()));
	}

}
