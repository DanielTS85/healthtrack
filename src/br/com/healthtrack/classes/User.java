package br.com.healthtrack.classes;
/**
 * Classe que abstrai os dados do usu�rio
 * @author SOS FIAP
 * @version 1.0
 */
public class User {

	/**
	 * Atributos de Classe
	 */
	
	/**
	 * Nome do usu�rio
	 */
	private String name;
	
	/**
	 * E-mail do usu�rio
	 */
	private  String email;
	
	/**
	 * Construtores
	 * de Classe
	 * @param aName O nome do usu�rio
	 * @param anEmail O e-mail do usu�rio
	 */
	public User (String aName, String anEmail) {
		this.name = aName;
		this.email = anEmail;
	}
	
	/**
	 * M�todos
	 */
	
	/**
	 * Visualizar dados do usu�rio
	 */
	public void show() {
		System.out.println(String.format("Dados do Usu�rio %s (%s)", name, email));
	}
	
	
	/**
	 * Atualizar dados do usu�rio
	 * @param aName O nome do usu�rio
	 * @param aEmail O e-mail do usu�rio
	 */
	public void update(String aName, String aEmail) {
		this.name = aName;
		this.email = aEmail;
		System.out.println(String.format("Novos dados do Usu�rio %s (%s)", name, email));
	}
	
}
