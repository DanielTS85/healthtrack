package br.com.healthtrack.classes;

public class ExerciseAerobic extends Exercise {
	
	private String speed;

	public ExerciseAerobic(String aName, int aRepetition, int aMoviment, String aSpeed) {
		super(aName, aRepetition, aMoviment, "Aer�bico");
		this.speed = aSpeed;
	}
	
	public String getSpeed() {
		return this.speed;
	}
	
	@Override
	public void show() {
		super.show();
		System.out.println(String.format("Velocidade: %s", getSpeed()));
	}

}
