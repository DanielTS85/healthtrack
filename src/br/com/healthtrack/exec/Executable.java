package br.com.healthtrack.exec;

import java.util.List;

import br.com.healthtrack.classes.Auth;
import br.com.healthtrack.classes.ExerciseAerobic;
import br.com.healthtrack.classes.ExerciseEquipment;
import br.com.healthtrack.classes.User;
import br.com.healthtrack.classes.Weight;
import br.com.healthtrack.model.WeightDAO;

/**
 * Classe que inicializa a aplica��o
 * @author SOS FIAP
 * @version 1.0
 */
public class Executable {
	
	/**
	 * M�todo main
	 * @param args Par�metros de entrada
	 */
	public static void main(String[] args) {		
		
		/**
		 * Teste de classe User
		 */
		System.out.println("Classe User:");
		User user = new User("FIAP", "FIAP@fiap.com.br");
		user.show();
		user.update("SOS FIAP", "sos@fiap.com.br");
		System.out.println("*=============================*");
		
		/**
		 * Teste de classe Weight
		 */
		System.out.println("Classe Weight:");
		Weight weight = new Weight();
		weight.setWeight(54.00);
		System.out.println(String.format("Peso %s cadastrado", weight.getWeight()));
		weight.updateWeight(60.00);
		System.out.println(String.format("Peso %s cadastrado", weight.getWeight()));
		weight.deleteWeight();
		System.out.println("*=============================*");
		
		/**
		 * Teste de classe Auth
		 */
		System.out.println("Classe Auth:");
		Auth auth = new Auth();
		auth.register("SOS", "sos@fiap.com.br", "123456");
		auth.login("sos@fiap.com.br", "123456");
		auth.logout();
		auth.forgotPassword("sos@fiap.com.br");
		System.out.println("*=============================*");

		/**
		 * Teste de classe Exercise
		 */
		System.out.println("Classe Exercise:");
		ExerciseEquipment equipment = new ExerciseEquipment("Barra fixa", 4, 20, 15.0);
		equipment.show();
		
		System.out.println(" ");
		
		ExerciseAerobic aerobic = new ExerciseAerobic("Flex�o", 5, 15, "R�pido");
		aerobic.show();
		System.out.println("*=============================*");
		
	}

}
