package br.com.healthtrack.exec;

import java.util.List;

import br.com.healthtrack.classes.Weight;
import br.com.healthtrack.model.WeightDAO;

public class Teste {
	public static void main(String[] args) {		
		
		/**
		 * Teste da classe WeightDAO
		 */
		System.out.println("Classe WeightDAO:");
		WeightDAO dao = new WeightDAO();
		
		List<Weight> listWeights = dao.getAll();
		for (Weight item : listWeights) {
			System.out.println(item);
		}

	}
}
