package br.com.healthtrack.model;

import java.util.ArrayList;
import java.util.List;

import br.com.healthtrack.classes.Weight;

public class WeightDAO {
	
	public WeightDAO() {
		
	}
	
	public List<Weight> getAll() {
		
		List<Weight> listWeight = new ArrayList<Weight>();
		
		for (int i = 0; i < 10; i++) {
			Weight weight = new Weight();
			weight.setWeight(54.00 + i);
			
			listWeight.add(weight);
		}
		
		return listWeight ;
	}
}
