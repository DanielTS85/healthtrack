package br.com.healthtrack.classes;
/**
 * Classe que abstrai os pesos cadastrados pelo usu�rio
 * @author SOS FIAP
 * @version 1.0
 */
public class Weight {

	/**
	 * Atributos de Classe
	 */
	
	/**
	 * Peso do usu�rio
	 */
	private double weight;
	
	
	
	public Weight() {}
	
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void updateWeight(double weight) {
		this.weight = weight;
		System.out.println(String.format("Peso %s atualizado", weight));
	}
	
	public void deleteWeight() {
		System.out.println("Peso removido");
	}
	
	@Override
	public String toString() {
		return "Weight [weight=" + weight + "]";
	}
	
}
