package br.com.healthtrack.classes;
/**
 * Classe que abstrai a autentica��o de usu�rios
 * @author SOS FIAP
 * @version 1.0
 */
public class Auth {
		
	/**
	 * Atributos de Classe
	 */
	
	/**
	 * Nome do usu�rio
	 */
	private String name;
	
	/**
	 * E-mail do usu�rio
	 */
	private String email;
	
	/**
	 * senha do usu�rio
	 */
	private String password;
	
	/**
	 * Construtor
	 */
	public Auth() {}
	
	/**
	 * M�todos
	 */
	
	/**
	 * Efetua o login no sistema
	 * @param email O e-mail do usu�rio
	 * @param password A senha do usu�rio
	 */
	public void login(String email, String password) {
		this.email = email;
		System.out.println(String.format("Usu�rio %s autenticado", this.email));
	}
	
	/**
	 * Efetua o logout no sistema
	 */
	public void logout() {
		System.out.println(String.format("Usu�rio %s desconectado", email));
	}
	
	/**
	 * Registro do usu�rio
	 * @param name O nome do usu�rio
	 * @param email O e-mail do usu�rio
	 * @param password A senha do usu�rio
	 */
	public void register(String name, String email, String password) {
		this.name = name;
		this.email = email;
		this.password = password;
		System.out.println(String.format("Usu�rio %s cadastrado", this.email));
	}
	
	/**
	 * Lembrete de senha
	 * @param email O e-mail do usu�rio
	 */
	public void forgotPassword(String email) {
		System.out.println(String.format("Usu�rio %s esqueceu a senha", email));
	}
}
